from django.core.management import call_command
from celery import shared_task



@shared_task
def backup_database():
    call_command('dbbackup', clean=True, compress=True)


@shared_task
def backup_media():
    call_command('mediabackup', clean=True)